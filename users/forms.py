from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser

class CustomUserCreationForm(UserCreationForm):

	username = forms.CharField(label = ('Tên người dùng'),widget = forms.TextInput(attrs={'class': 'input is-primary', 'placeholder': 'tên...'}))
	email = forms.EmailField(label = ('Địa chỉ email'), widget = forms.EmailInput(attrs={'class': 'input is-info', 'placeholder': 'tên@...'}))
	phone_number  = forms.CharField(label =('Số điện thoại'), widget = forms.TextInput(attrs = {'class':'input is-warning'}))
	address  = forms.CharField(label =('Địa chỉ'), widget = forms.TextInput(attrs = {'class':'input is-primary', 'placeholder':'Số .. đường ..'}))
	password1 = forms.CharField(widget = forms.PasswordInput(attrs={'class': 'input', 'placeholder': '******'}))
	password2 = forms.CharField(widget = forms.PasswordInput(attrs={'class': 'input', 'placeholder': '******'}))
	lat = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lat'}))
	lng = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lng'}))

	class Meta(UserCreationForm.Meta):
		model = CustomUser
		
		fields = UserCreationForm.Meta.fields + ('email', 'phone_number', 'address','lat','lng')


	def clean_password2(self):
		cd = self.cleaned_data
		if cd['password1'] != cd['password2']:
			raise forms.ValidationError('Mật khẩu không khớp')
		return cd['password1']

	# def clean_email(self):

	# 	email = self.cleaned_data['email']

	# 	if CustomUser.objects.filter(email=email).exists():
	# 		raise forms.ValidationError("email này đã tồn tại")


	# 	return email

class CustomUserChangeForm(UserChangeForm):

	class Meta:
		model = CustomUser
		fields = ('username', 'email')
