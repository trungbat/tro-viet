L.MakiMarkers.accessToken = "pk.eyJ1IjoidHJ1bmdiYXQiLCJhIjoiY2ptdWllZTY4MDlhNjNwcGtqY2FjaGtkeCJ9.F51_3qvVuNyo_C17deJb5A"

var map = L.map('map').setView([20.9894757,105.8523328], 8);

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

var searchControl = L.esri.Geocoding.geosearch().addTo(map);


var results = L.layerGroup().addTo(map);

searchControl.on('results', function(data){
  
    results.clearLayers();
    for (var i = data.results.length - 1; i >= 0; i--) {

    	 var m1 = L.marker(data.results[i].latlng, {draggable:true, icon:icon})
       results.addLayer(m1);

      	m1.on('drag', function(e){
      		document.getElementById('lat').value = e.latlng['lat'];
      		document.getElementById('lng').value = e.latlng['lng']
      	})

//       	m1.on('drag',function(e){
//  			var latlng = e.getLatLng();
//     		document.getElementById('lat').innerHTML = latlng.lat;
//     		document.getElementById('lng').innerHTML = latlng.lng;

// });
    }


  });

var icon = L.MakiMarkers.icon({icon: "rocket", color: "#ff0537", size: "3"});


// var marker4 = L.marker([20.9894757,105.8523328],
// {title:"marrker4",alt:"The Big I", opacity: 8, icon: icon}).addTo(map);

