from django.db import models
from django.conf import settings
from django.utils import timezone
from django.core.validators import RegexValidator
from django.urls import reverse
import json

class Nha(models.Model):
	CATEGORY_CHOICES = (
		('cc','Chung chủ'),
		('rc','Riêng chủ'),
	)

	author = models.ForeignKey(settings.AUTH_USER_MODEL,
							related_name= 'tao_nha',
							on_delete= models.CASCADE)

	description = models.TextField(('Miêu tả'),)
	price = models.DecimalField(('Giá phòng'),max_digits=5, decimal_places=3)
	category = models.CharField(('Loại phòng'),max_length = 15, choices = CATEGORY_CHOICES, default = 'rieng')
	phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Điện thoại phải ở định dạng: '+999999999'. Chỉ chứ tối đa 15 số.")
	phone_number = models.CharField(('Số điện thoại') ,validators=[phone_regex], max_length=17, blank=True) 
	address = models.CharField(('Địa chỉ'), max_length = 100)
	lat = models.DecimalField(max_digits = 18, decimal_places = 15, blank  = True, null = True)
	lng = models.DecimalField(max_digits = 18, decimal_places = 15, blank  = True, null = True)
	publish = models.DateTimeField(default=timezone.now)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	class Meta:
		ordering = ('-publish',)
		verbose_name_plural = 'Nhà'

	def get_absolute_url(self):
		return reverse('nha_list')
 

	def __str__(self):
		return self.address

class Images(models.Model):

	nha = models.ForeignKey(Nha, verbose_name = 'Nhà', on_delete =models.CASCADE)
	image = models.ImageField(upload_to = 'images/', verbose_name = 'Hình ảnh', blank=True, null= True)

	def __str__(self):
		return self.nha.description[:15] +'Image'