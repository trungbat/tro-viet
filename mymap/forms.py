from django import forms
from .models import Nha
from django.views.generic.edit import UpdateView

class NhaCreateForm(forms.ModelForm):
	CATEGORY_CHOICES = (
		('rc','Riêng chủ'),
		('cc','Chung chủ'),
	)
	description = forms.CharField(widget = forms.Textarea(attrs={'class': 'textarea is-primary', 'placeholder': 'Miêu tả phòng...','rows':"3"}))
	category = forms.CharField(widget = forms.Select(attrs= {'class':'input '}, choices=CATEGORY_CHOICES))
	phone_number  = forms.CharField(label =('Số điện thoại'), widget = forms.TextInput(attrs = {'class':'input is-warning'}))
	address  = forms.CharField(label =('Địa chỉ'), widget = forms.TextInput(attrs = {'class':'input is-primary', 'placeholder':'Số .. đường ..'}))
	lat = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lat'}))
	lng = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lng'}))
	price = forms.DecimalField(widget = forms.NumberInput(attrs = {'id':'demo','class':'input'}) ,max_digits=5, decimal_places=3)



	class Meta:
		model = Nha 
		fields  = ('description','price','category','phone_number','address','lat','lng')




class SearchForm(forms.ModelForm):
	CATEGORY_CHOICES = (
		('rc','Riêng chủ'),
		('cc','Chung chủ'),
	)
	lat = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lat'}))
	lng = forms.CharField(widget = forms.HiddenInput(attrs= {'class': 'input', 'id':'lng'}))
	price = forms.DecimalField(widget = forms.NumberInput(attrs = {'id':'demo','class':'input'}) ,max_digits=5, decimal_places=3)
	category = forms.CharField(widget = forms.Select(attrs= {'class':'input '}, choices=CATEGORY_CHOICES))
	

	class Meta:
		model = Nha 
		fields = ('category', 'price','lat','lng')