from django.contrib import admin
from .models import Nha

class NhaAdmin(admin.ModelAdmin):
	list_display = ('description','price','category','address')
	list_filter  = ('category','created','publish')
	date_hierarchy = 'publish'
	ordering = ('price','publish')

admin.site.register(Nha, NhaAdmin)