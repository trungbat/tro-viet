from django.shortcuts import render, redirect, get_object_or_404

from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
from django.views.generic.edit import UpdateView
from .forms import NhaCreateForm, SearchForm
from .models import Nha
from django.core import serializers
import geopy.distance



@login_required
def NhaListView(request):
	nhas = Nha.objects.filter(author = request.user)
	# nha_data = list(Nha.objects.values_list('description','price','category','phone_number','address','lat','lng'))
	nha_data = serializers.serialize('json', nhas)

	return render(request,'mymap/nha_list.html',
					{'nhas':nhas, 'nha_data':nha_data}
				)


@login_required
def NhaCreateview(request):
	if request.method == 'POST':
		form = NhaCreateForm(data=request.POST)
		if form.is_valid():
			nha = form.save(commit=False)
			nha.author = request.user
			nha.save()
			return redirect('nha_list')
	else:
		form = NhaCreateForm(initial={"phone_number": request.user.phone_number,
								'address':request.user.address,
								'lat':request.user.lat,
								'lng':request.user.lng,
							})

	return render(request, 'mymap/nha_register.html',{'form':form})
	

class EditNhaView(UpdateView):
	model = Nha
	form_class = NhaCreateForm
	# fields = ['description', 'category',  'price', 'phone_number', 'address', 'lat', 'lng']
	template_name = 'mymap/nha_edit.html'



def ketQuaTimKiem(user_latlng, category, price, id_list):
	nha_latlng = []
	nha_data = Nha.objects.filter(category = category, price__lte = price).values_list('lat','lng','id')
	
	for i in nha_data:
		nha_latlng.append([float(i[0]) , float(i[1])])
		id_list.append(i[2]) 

	return list(x for x in nha_latlng if (geopy.distance.vincenty(x, user_latlng).km <5.0))


def SearchView(request):
	id_list = []
	nha_data = None
	if request.method == 'POST':
		form = SearchForm(data=request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			category = cd['category']
			price = cd['price']
			user_latlng = [float(cd['lat']), float(cd['lng'])]

			toa_do_list = ketQuaTimKiem(user_latlng, category,price, id_list)
			nhas = Nha.objects.filter(id__in = id_list)
			print(nhas.count())
			nha_data = serializers.serialize('json', nhas)

			return render(request,'mymap/tim_nha.html', {'form':form, 'toa_do_list':toa_do_list, 'dem':len(toa_do_list), 'nha_data':nha_data, 'nhas':nhas})

	else:
		form = SearchForm(initial={
			'lat':request.user.lat,
			'lng':request.user.lng,})



	return render(request, 'mymap/tim_nha.html', {'form':form, 'toa_do_list': None, 'nha_data':nha_data})


def DevelopView(request):
	return render(request, 'develop.html')

def DeleteNhaview(request,pk):
	nha  = get_object_or_404(Nha, pk =pk)
	nha.delete()

	return redirect('nha_list')

