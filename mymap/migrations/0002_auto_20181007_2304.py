# Generated by Django 2.0.5 on 2018-10-07 16:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mymap', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='nha',
            options={'ordering': ('-publish',), 'verbose_name_plural': 'Nhà'},
        ),
    ]
